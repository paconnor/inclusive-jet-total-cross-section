We calculate the integrated cross section of inclusive jet production for jet clustered with the anti-kt jet algorithm in a common phase space selection ($p_\mathrm{T} > 133 \text{GeV}$ and $|y| < 2.0$).

|            |  lumi    | R   | CADI       | arXiv                                          | HEPdata                                              | $\sigma$ |
| ---        |  ---     | --- | ---        | ---                                            | ---                                                  | ---      |
| 2.76 TeV   |  5.4/pb  | 7   | SMP-14-017 | [1512.06212](https://arxiv.org/abs/1512.06212) | [1410826](https://www.hepdata.net/record/ins1410826) | $787.3\pm3.6\text{(stat)}\pm16.2\text{(syst)}$ |
| 5.02 TeV   |  27.4/pb | 4   | SMP-21-009 | [2401.11355](https://arxiv.org/abs/2401.11355) | [2750408](https://www.hepdata.net/record/ins2750408) |  |
| 7 TeV      |  5.0/fb  | 7   | SMP-12-018 | [1212.6660 ](https://arxiv.org/abs/1212.6660)  | [1208923](https://www.hepdata.net/record/ins1208923) | $8519\pm41\text{(stat)}\pm185\text{(syst)}$ |
| 8 TeV      |  20/fb   | 7   | SMP-14-001 | [1609.05331](https://arxiv.org/abs/1609.05331) | [1487277](https://www.hepdata.net/record/ins1487277) | $11220\pm17\text{(stat)}\pm183\text{(syst)}$ |
| 13 TeV     |  36.3/fb | 4   | SMP-20-011 | [2111.10431](https://arxiv.org/abs/2111.10431) | [1972986](https://www.hepdata.net/record/ins1972986) |  |
| 13 TeV     |  33.2/fb | 7   | SMP-20-011 | [2111.10431](https://arxiv.org/abs/2111.10431) | [1972986](https://www.hepdata.net/record/ins1972986) | $15230\pm36\text{(stat)}\pm269\text{(syst)}$ |

To reproduce the values reported in the table, the ROOT files should be downloaded by hand from the linked HEP data record, then the scripts should be run with `root -l -b -q [name].C`.

Note: the 5-TeV analysis is using a bining that does not match with the other measurements and is the only analysis not providing a value for $R=0.7$, therefore a comparable value may not be provided.
