{
    TFile::Open("HEPData-ins1410826-v1-root.root");
    double sigm = 0, stat2 = 0, syst2 = 0;
    for (int y = 1; y <= 4; ++y) {
        gFile->cd(Form("Table %d", y));
        auto h_sigm = gDirectory->Get<TH1>("Hist1D_y1"),
             h_stat = gDirectory->Get<TH1>("Hist1D_y1_e1"),
             h_syst = gDirectory->Get<TH1>("Hist1D_y1_e2");
        cout << h_sigm << ' ' << h_stat << ' ' << h_syst << endl;
        int I = h_sigm->FindBin(133),
            N = h_sigm->GetNbinsX();
        for (int i = I; i <= N; ++i) {
            sigm  +=     h_sigm->GetBinContent(i);
            stat2 += pow(h_stat->GetBinContent(i),2);
            syst2 += pow(h_syst->GetBinContent(i),2);
        }
    }
    cout << sigm << ' ' << sqrt(stat2) << ' ' << sqrt(syst2) << endl;
}
